import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'package:app_turismo_pasaje/apps/ListaServicios.dart';

class CarouselLugaresInteresWidget extends StatefulWidget {
  const CarouselLugaresInteresWidget({
    Key key,
    @required this.lugares,
    @required this.pathImages,
  }) : super(key: key);

  final List lugares;
  final String pathImages;

  @override
  CarouselLugaresInteresWidgetState createState() => CarouselLugaresInteresWidgetState();
}

class CarouselLugaresInteresWidgetState extends State<CarouselLugaresInteresWidget> {
  //
  CarouselSlider carouselSlider;
  int _current = 0;
  List lugares;
  String pathImages;

  @override
  void initState() {
    super.initState();
    lugares = widget.lugares;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              carouselSlider = CarouselSlider(
                height: 200.0,
                initialPage: 0,
                enlargeCenterPage: true,
                autoPlay: true,
                reverse: false,
                enableInfiniteScroll: lugares.length > 2 ? true : false,
                autoPlayInterval: Duration(seconds: 2),
                autoPlayAnimationDuration: Duration(seconds: 3),
                pauseAutoPlayOnTouch: Duration(seconds: 10),
                scrollDirection: Axis.horizontal,
                onPageChanged: (index) {
                  setState(() {
                    _current = index;
                  });
                },
                items: lugares.map((lugar) {
                  return Builder(
                    builder: (BuildContext context) {
                      return GestureDetector(
                        onTap: () {
//                          Navigator.of(context).push(
//                              MaterialPageRoute(
//                                builder: (context) => ListaServicios(data: services.where((service) => service.categoria == categoria["categoria"]).toList(), pathImages: pathImages,),
//                              )
//                          );
                        },
                        child: Card(
                          elevation: 2,
                          clipBehavior: Clip.hardEdge,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7)
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
                          child: Stack(
                            fit: StackFit.expand,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    color: Colors.blueAccent,
                                ),
                                child: FadeInImage.assetNetwork(
                                  placeholder: 'assets/loading-places.gif',
                                  image: "$pathImages/img/${lugar.imagen}",
                                  fit: BoxFit.cover,

                                ),
                              ),
                              Positioned(
                                child: Padding(
                                  padding: EdgeInsets.all(0),
                                  child: Container(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomLeft,
                                            stops: [0, .7],
                                            colors: [Colors.white.withOpacity(0), Colors.black.withOpacity(.5)]
                                        )
                                    ),
                                  ),
                                ),
                                bottom: 0,
                              ),
                              Positioned(
                                child: Padding(
                                  padding: EdgeInsets.all(15),
                                  child: Text(lugar.nombre, style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                    fontSize: 15,
                                  )),
                                ),
                                bottom: 0,
                              ),
                            ],
                          )
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
            ],
          ),
        ),
        Positioned(
          top: 0,
          child: Container(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: MediaQuery.of(context).size.height / 2,
                width: 40,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.bottomRight,
                        stops: [0.1, 1],
                        colors: [Colors.white.withOpacity(0.4), Colors.white.withOpacity(0)]
                    )
                ),
              ),
            ),

          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: Container(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: MediaQuery.of(context).size.height / 2,
                width: 40,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomRight,
                        end: Alignment.bottomLeft,
                        stops: [0.1, 1],
                        colors: [Colors.white.withOpacity(0.4), Colors.white.withOpacity(0)]
                    )
                ),
              ),
            ),

          ),
        ),
      ],
    );
  }
}