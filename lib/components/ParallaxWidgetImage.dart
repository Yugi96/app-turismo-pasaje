import 'package:flutter/material.dart';

class ParallaxWidgetImage extends StatelessWidget {
  const ParallaxWidgetImage({
    Key key,
    @required this.top,
    @required this.asset,
  }) : super(key: key);

  final double top;
  final String asset;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Image.asset("assets/${asset}.png", fit: BoxFit.cover),
      ),
    );
  }
}