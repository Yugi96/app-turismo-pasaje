import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImageSelectedWidget extends StatefulWidget {
  const ImageSelectedWidget({
    Key key,
    @required this.pathImage,
  }) : super(key: key);

  final String pathImage;

  @override
  ImageSelectedWidgetState createState() =>ImageSelectedWidgetState();
}

class ImageSelectedWidgetState extends State<ImageSelectedWidget> {
  String pathImage;

  @override
  void initState() {
    super.initState();
    pathImage = widget.pathImage;
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Galería"),
      ),
      body: Container(
        child: PhotoView(
          imageProvider: NetworkImage(pathImage),
          backgroundDecoration: BoxDecoration(color: Colors.white),
          minScale: PhotoViewComputedScale.contained,
        ),
      ),
    );
  }
}
