import 'package:app_turismo_pasaje/apps/ParroquiaSeleccionada.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CarouselParroquiasWidget extends StatefulWidget {
  const CarouselParroquiasWidget({
    Key key,
    @required this.data,
    @required this.pathImages,
  }) : super(key: key);

  final List data;
  final String pathImages;

  @override
  CarouselParroquiasWidgetState createState() => CarouselParroquiasWidgetState();
}

class CarouselParroquiasWidgetState extends State<CarouselParroquiasWidget> {
  //
  CarouselSlider carouselSlider;
  int _current = 0;
  List data;
  String pathImages;

  @override
  void initState() {
    super.initState();
    data = widget.data;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              carouselSlider = CarouselSlider(
                height: 200.0,
                initialPage: 0,
                enlargeCenterPage: true,
                autoPlay: true,
                reverse: false,
                enableInfiniteScroll: data.length > 2 ? true : false,
                autoPlayInterval: Duration(seconds: 2),
                autoPlayAnimationDuration: Duration(seconds: 3),
                pauseAutoPlayOnTouch: Duration(seconds: 10),
                scrollDirection: Axis.horizontal,
                onPageChanged: (index) {
                  setState(() {
                    _current = index;
                  });
                },
                items: data.map((parroquia) {
                  return Builder(
                    builder: (BuildContext context) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => ParroquiaSeleccionada(
                                data: parroquia, pathImages: pathImages),
                          ));
                        },
                        child: Card(
                          elevation: 2,
                          clipBehavior: Clip.hardEdge,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(7)
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
                          child: Stack(
                            fit: StackFit.expand,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                    color: Colors.blueAccent,
                                ),
                                child: FadeInImage.assetNetwork(
                                  placeholder: 'assets/loading-places.gif',
                                  image: "$pathImages/img/${parroquia.imagen}",
                                  fit: BoxFit.cover,

                                ),
                              ),
                              Positioned(
                                child: Padding(
                                  padding: EdgeInsets.all(0),
                                  child: Container(
                                    height: 100,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topLeft,
                                            end: Alignment.bottomLeft,
                                            stops: [0, .7],
                                            colors: [Colors.white.withOpacity(0), Colors.black.withOpacity(.5)]
                                        )
                                    ),
                                  ),
                                ),
                                bottom: 0,
                              ),
                              Positioned(
                                child: Padding(
                                  padding: EdgeInsets.all(15),
                                  child: Text(parroquia.nombre, style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w300,
                                    fontSize: 17,
                                  )),
                                ),
                                bottom: 0,
                              ),
                            ],
                          )
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
            ],
          ),
        ),
        Positioned(
          top: 0,
          child: Container(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: MediaQuery.of(context).size.height / 2,
                width: 40,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.bottomRight,
                        stops: [0.1, 1],
                        colors: [Colors.white.withOpacity(0.4), Colors.white.withOpacity(0)]
                    )
                ),
              ),
            ),

          ),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: Container(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: MediaQuery.of(context).size.height / 2,
                width: 40,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomRight,
                        end: Alignment.bottomLeft,
                        stops: [0.1, 1],
                        colors: [Colors.white.withOpacity(0.4), Colors.white.withOpacity(0)]
                    )
                ),
              ),
            ),

          ),
        ),
      ],
    );
  }
}