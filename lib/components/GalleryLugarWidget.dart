import 'package:flutter/material.dart';
import 'package:app_turismo_pasaje/components/ImageSelected.dart';

class GalleryLugarWidget extends StatefulWidget {
  const GalleryLugarWidget({
    Key key,
    @required this.data,
    @required this.pathImages,
  }) : super(key: key);

  final List data;
  final String pathImages;

  @override
  GalleryLugarWidgetState createState() =>GalleryLugarWidgetState();
}


class GalleryLugarWidgetState extends State<GalleryLugarWidget> {
  List data;
  String pathImages;

  @override
  void initState() {
    super.initState();
    data = widget.data;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - 380,
      child: CustomScrollView(
        slivers: <Widget>[
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ImageSelectedWidget(
                        pathImage: "$pathImages/img/${data[index]["imagen"]}",
                  )
                  )
                  );},
                child: Card(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(color: Colors.blueAccent),
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/loading-places.gif',
                      image: "$pathImages/img/${data[index]["imagen"]}",
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              );
            },
            childCount:  data.length
          )
          )
//          SliverFixedExtentList(
//            itemExtent: 140,
//            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
//              return GestureDetector(
//                onTap: (){
//
//                },
//                child: Card(
//                  child: Container(
//                    width: MediaQuery.of(context).size.width,
//                    decoration: BoxDecoration(color: Colors.blueAccent),
//                    child: FadeInImage.assetNetwork(
//                      placeholder: 'assets/loading-places.gif',
//                      image: "$pathImages/img/${data[index]["imagen"]}",
//                      fit: BoxFit.cover,
//                    ),
//                  ),
//                ),
//              );
//            }, childCount:  data.length),
//          )
        ],
      )
    );
  }
}