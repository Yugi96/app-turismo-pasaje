import 'package:app_turismo_pasaje/models/turismo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:app_turismo_pasaje/components/GalleryLugarWidget.dart';


class LugarSeleccionado extends StatefulWidget {
  const LugarSeleccionado(
      {Key key, @required this.data, @required this.pathImages})
      : super(key: key);

  final Turismo data;
  final String pathImages;

  @override
  _LugarSeleccionadoState createState() => _LugarSeleccionadoState();
}

class _LugarSeleccionadoState extends State<LugarSeleccionado> {
  Turismo data;
  String pathImages;
  List galery;

  double topOne = 0;
  double topTwo = 0;
  double topThree = 0;
  bool isChange = false;
  bool isVisible = false;
  bool mapVisible = true;
  bool descVisible = false;
  bool galVisible = false;

  @override
  void initState() {
    super.initState();
    data = widget.data;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    galery = data.galeria.where((img) => img["estado"]==1).toList();
    return Scaffold(
      appBar: AppBar(
        title: Text(data.nombre),
      ),
      backgroundColor: Colors.white,
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification && v.depth == 0) {
            setState(() {
              topOne = topOne - v.scrollDelta / 1;
              topTwo = topTwo - v.scrollDelta / 3;
              topThree = topThree - v.scrollDelta / 1;
            });
            isChange = v.metrics.pixels > 200;
            isVisible = v.metrics.pixels > 200;
          }
          return true;
        },
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 300,
                  decoration: BoxDecoration(
                    color: Colors.blueAccent,
                  ),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/loading-places.gif',
                    image: "$pathImages/img/${data.galeria[0]["imagen"]}",
                    fit: BoxFit.cover,

                  ),
//                  decoration: BoxDecoration(
//                      color: Colors.transparent,
//                      image: DecorationImage(
//                        image: NetworkImage(
//                          "$pathImages/img/${data.galeria[0]["imagen"]}",
//                        ), fit: BoxFit.cover,
//                      )),
                ),
                Container(
                  height: 300,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.black.withOpacity(0.3), Colors.white.withOpacity(0.5)],
                        stops: [0.2, 1.0],
                      )),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 280,
                        child: Wrap(
                          runAlignment: WrapAlignment.end,
                          spacing: 50,
                          children: <Widget>[
                            IconButton(
                              color: Colors.white,
                              icon: Icon(Icons.assignment),
                              onPressed: () {
                                setState(() {
                                  mapVisible = false;
                                  galVisible = false;
                                  descVisible = true;
                                });
                              },
                            ),
                            IconButton(
                              color: Colors.white,
                              icon: Icon(Icons.photo_library),
                              onPressed: () {
                                setState(() {
                                  mapVisible = false;
                                  galVisible = true;
                                  descVisible = false;
                                });
                              },),
                            IconButton(
                              color: Colors.white,
                              icon: Icon(Icons.location_on),
                              onPressed: () {
                                setState(() {
                                  mapVisible = true;
                                  galVisible = false;
                                  descVisible = false;
                                });
                              },),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            descVisible ? Container(
              height: MediaQuery.of(context).size.height - 380,
              color: const Color(0xf9f7f7),
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildListDelegate([
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                    Icons.map,
                                    color: Colors.blueAccent                          ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    'Ubicación',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17,
                                        color: Colors.blueAccent                              ),
                                  ),
                                ),
                              ],
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 3),
                              child: Text(
                                  '${data.ubicacion}'
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                    Icons.layers,
                                    color: Colors.blueAccent
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    'Temperatura',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17,
                                        color: Colors.blueAccent
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 3),
                              child: Text(
                                  '${data.temperatura}'
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                    Icons.person_pin,
                                    color: Colors.blueAccent
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    'Descripción',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17,
                                        color: Colors.blueAccent
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 3, left: 10, right: 10),
                              child: Text(
                                '${data.descripcion}'
                                , textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                    Icons.directions,
                                    color: Colors.blueAccent
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    'Distancia',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17,
                                        color: Colors.blueAccent
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 3),
                              child: Text(
                                  '${data.distancia}'
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Column(
                          children: <Widget>[
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                    Icons.nature_people,
                                    color: Colors.blueAccent
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 5),
                                  child: Text(
                                    'Actividades',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17,
                                        color: Colors.blueAccent
                                    ),
                                  ),
                                ),
                              ],
                            ),

                            Padding(
                              padding: const EdgeInsets.only(top: 3),
                              child: Text(
                                  '${data.actividades.map((actividad) => actividad["nombre"]).toString()}'
                              ),
                            ),
                          ],
                        ),
                      ),
                    ]),
                  )
                ],
              ),
            )
              : SizedBox(height: 0,),
            galVisible ? GalleryLugarWidget(data: galery,pathImages: pathImages,) : SizedBox(height: 0),
            mapVisible ? Container(
              height: MediaQuery.of(context).size.height - 380,
              child: FlutterMap(
                options: new MapOptions(
                    center: new LatLng(double.parse(data.latitud), double.parse(data.longitud)),
                    zoom: 13.0,
                    interactive: true
                ),
                layers: [
                  new TileLayerOptions(
                    urlTemplate: "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}",
                    additionalOptions: {
                      'accessToken': 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
                      'id': 'mapbox.streets',
                    },
                  ),
                  new MarkerLayerOptions(
                    markers: [
                      new Marker(
                        width: 80.0,
                        height: 80.0,
                        point: new LatLng(double.parse(data.latitud), double.parse(data.longitud)),
                        builder: (ctx) =>
                        new Container(
                          child: new Icon(Icons.location_on, color: Colors.red, size: 50,),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ): SizedBox(height: 0,),
          ],
        ),
      ),
    );
  }
}
