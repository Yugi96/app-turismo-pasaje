import 'package:app_turismo_pasaje/models/turismo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

import 'package:app_turismo_pasaje/models/parroquia.dart';

class ParroquiaSeleccionada extends StatefulWidget {
  const ParroquiaSeleccionada(
      {Key key, @required this.data, @required this.pathImages})
      : super(key: key);

  final Parroquia data;
  final String pathImages;

  @override
  _ParroquiaSeleccionadaState createState() => _ParroquiaSeleccionadaState();
}

class _ParroquiaSeleccionadaState extends State<ParroquiaSeleccionada> {
  Parroquia data;
  String pathImages;

  @override
  void initState() {
    super.initState();
    data = widget.data;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 300,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  color: Colors.blueAccent,
                ),
                child: FadeInImage.assetNetwork(
                  placeholder: 'assets/loading-places.gif',
                  image: "$pathImages/img/${data.imagen}",
                  fit: BoxFit.cover,

                ),
//                  decoration: BoxDecoration(
//                      color: Colors.transparent,
//                      image: DecorationImage(
//                        image: NetworkImage(
//                          "$pathImages/img/${data.galeria[0]["imagen"]}",
//                        ), fit: BoxFit.cover,
//                      )),
              ),
              Container(
                height: 300,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.black.withOpacity(0.3), Colors.white.withOpacity(0.4), Colors.white],
                      stops: [0.6, .8, .9],
                    )),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            data.nombre,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 32,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 1.3
                            ),
                          ),
                          Text(
                            data.fundacion,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 1.3
                            )
                          ),
                        ]
                      ),
                    ),
//                  IconButton(icon: Icon(Icons.photo_library, color: Colors.indigo,), onPressed: null)
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: MediaQuery.of(context).size.height - 300,
            color: const Color(0xf9f7f7),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.map,
                              color: Colors.blueAccent                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                                'Ubicación',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                  color: Colors.blueAccent                              ),
                            ),
                          ),
                        ],
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: Text(
                          '${data.ubicacion}'
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                              Icons.layers,
                              color: Colors.blueAccent
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                                'Superficie',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                  color: Colors.blueAccent
                              ),
                            ),
                          ),
                        ],
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: Text(
                            '${data.superficie}'
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                              Icons.person_pin,
                              color: Colors.blueAccent
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                                'Población',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                  color: Colors.blueAccent
                              ),
                            ),
                          ),
                        ],
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: Text(
                            '${data.poblacion}'
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                              Icons.directions,
                              color: Colors.blueAccent
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                                'Distancia de pasaje',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                  color: Colors.blueAccent
                              ),
                            ),
                          ),
                        ],
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: Text(
                            '${data.distancia_de_pasaje}'
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                              Icons.nature_people,
                              color: Colors.blueAccent
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                                'Actividades',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                  color: Colors.blueAccent
                              ),
                            ),
                          ),
                        ],
                      ),

                      Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: Text(
                            '${data.actividades}'
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
