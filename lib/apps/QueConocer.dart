import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:app_turismo_pasaje/components/CarouselWidget.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetImage.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetText.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetLinearGradient.dart';
import 'package:app_turismo_pasaje/models/turismo.dart';
import 'package:app_turismo_pasaje/apps/ListaLugares.dart';
import 'package:app_turismo_pasaje/components/loader.dart';

class QueConocer extends StatefulWidget {
  const QueConocer({
    Key key,
    @required this.apiBaseUrl,
  }) : super(key: key);

  final String apiBaseUrl;

  @override
  _QueConocer createState() => _QueConocer();
}

class _QueConocer extends State<QueConocer> {
  double topOne = 0;
  double topTwo = 0;
  double topThree = 0;
  bool isChange = false;
  bool isVisible = false;

  int count = 0;

  String url = "";
  List data;
  List importantPlaces;

  @override
  void initState() {
    super.initState();
    count = 0;
    url = "${widget.apiBaseUrl}/api/turismo";
  }

  Future<List<Turismo>> _getPlaces() async {
    if (data == null && count == 0) {
      count++;
      var response = await http.get(
          Uri.encodeFull(url),
          headers: { "Accept": "application/json" }
      );

      var convertDataToJson = jsonDecode(response.body);

      List<Turismo> turismoList = [];

      for (var t in convertDataToJson) {
        Turismo turismo = Turismo(t["id"], t["nombre"], t["descripcion"], t["ubicacion"], t["distancia"], t["temperatura"], t["longitud"], t["latitud"], t["es_importante"], t["galeria"], t["actividades"], t["parroquia"], t["estado"]);
        turismoList.add(turismo);
      }

      data = turismoList.where((turis) => turis.estado == 1).toList();
      return turismoList;
    } else {
      return data;
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification && v.depth == 0) {
            setState(() {
              topOne = topOne - v.scrollDelta / 1;
              topTwo = topTwo - v.scrollDelta / 3;
              topThree = topThree - v.scrollDelta / 1;
            });
            isChange = v.metrics.pixels > 200;
            isVisible = v.metrics.pixels > 200;
          }
          return true;
        },
        child: Stack(
          children: <Widget>[
            new ParallaxWidgetImage(top: topTwo, asset: "A"),
            new ParallaxWidgetLinearGradient(top: topThree, isVisible: isVisible),
            new ParallaxWidgetText(top: topOne, isChange: isChange, title: "¿Qué conocer?", description: "Información de las distintas actividades desarrolladas en la ciudad de Pasaje junto con sus atractivos turísticos entre los que se destacan parques, monumentos, sitios naturales y arqueológicos.",),
            ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent,
                ),
                FutureBuilder(
                  future: _getPlaces(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {

                    if (snapshot.data == null) {
                      return Container(
                        padding: EdgeInsets.only(top: 5, bottom: 15),
                        color: Colors.white,
                        width: double.infinity,
                        child: Center(
                            child: Column(
                              children: <Widget>[
                                Loader(),
                                SizedBox(
                                  height: 5,
                                ),
//                                Text("Cargando Lugares turísticos.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
//                                Text("Espere por favor.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
                              ],
                            )
                        ),
                      );
                    } else {
                      List<Turismo> turismo = snapshot.data;
                      data = turismo.where((turis) => turis.estado == 1).toList();
                      importantPlaces = turismo.where((Turismo place) => place.es_importante == 1 && place.estado == 1).toList();

                      return Container(
                        color: Colors.white,
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            importantPlaces == null || importantPlaces.length == 0 ? Text("") :
                            Padding(
                              padding: EdgeInsets.only(left: 20.0),
                              child: Text(
                                "RECOMENDACIONES",
                                style: TextStyle(
                                  fontSize: 16,
                                  letterSpacing: 1.2,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            importantPlaces.length == 0 ? Text("") : CarouselWidget(data: importantPlaces, pathImages: widget.apiBaseUrl,),
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: RaisedButton(
                                      color: Colors.blue,
                                      textColor: Colors.white,
                                      disabledColor: Colors.grey,
                                      disabledTextColor: Colors.black,
                                      padding: EdgeInsets.symmetric(horizontal: 50),
                                      splashColor: Colors.blueAccent,
                                      elevation: 2,
                                      onPressed: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) => ListaLugares(data: data, pathImages: widget.apiBaseUrl,),
                                            )
                                        );
                                      },
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "VER TODOS",
                                            style: TextStyle(fontSize: 12.0),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Icons.add,
                                            size: 16,
                                          )
                                        ],
                                      )
                                  ),
                                )

                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}