import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:app_turismo_pasaje/components/CarouselLugaresInteresWidget.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetImage.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetText.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetLinearGradient.dart';
import 'package:app_turismo_pasaje/models/agenda.dart' as AgendaModel;
import 'package:app_turismo_pasaje/components/loader.dart';

import 'package:app_turismo_pasaje/components/ImageSelected.dart';

import 'ListaServicios.dart';

class Agenda extends StatefulWidget {
  const Agenda({
    Key key,
    @required this.apiBaseUrl,
  }) : super(key: key);

  final String apiBaseUrl;

  @override
  _Agenda createState() => _Agenda();
}

class _Agenda extends State<Agenda> {
  double topOne = 0;
  double topTwo = 0;
  double topThree = 0;
  bool isChange = false;
  bool isVisible = false;

  int count = 0;

  String url = "";
  List dataAgenda;

  @override
  void initState() {
    super.initState();
    count = 0;
    url = "${widget.apiBaseUrl}/api/agendas";
  }

  Future<List<AgendaModel.Agenda>> _getAgendas() async {
    if (dataAgenda == null && count == 0) {
      count++;
      var response = await http.get(
          Uri.encodeFull(url),
          headers: { "Accept": "application/json" }
      );

      var convertDataToJson = jsonDecode(response.body);

      List<AgendaModel.Agenda> agendaList = [];

      for (var s in convertDataToJson) {
        AgendaModel.Agenda agenda = AgendaModel.Agenda(s["id"], s["nombre"], s["imagen"], s["estado"]);

        agendaList.add(agenda);
      }
      dataAgenda = agendaList.where((lugar) => lugar.estado == 1).toList();

      return agendaList;
    } else {
      return dataAgenda;
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification && v.depth == 0) {
            setState(() {
              topOne = topOne - v.scrollDelta / 1;
              topTwo = topTwo - v.scrollDelta / 3;
              topThree = topThree - v.scrollDelta / 1;
            });
            isChange = v.metrics.pixels > 200;
            isVisible = v.metrics.pixels > 200;
          }
          return true;
        },
        child: Stack(
          children: <Widget>[
            new ParallaxWidgetImage(top: topTwo, asset: "E"),
            new ParallaxWidgetLinearGradient(top: topThree, isVisible: isVisible),
            new ParallaxWidgetText(top: topOne, isChange: isChange, title: "Agenda", description: "Información de los eventos a realizarse en todas sus categorías y datos de los principales proyectos.",),
            ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent,
                ),
                FutureBuilder(
                  future: _getAgendas(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {

                    if (snapshot.data == null && dataAgenda == null) {
                      return Container(
                        padding: EdgeInsets.only(top: 5, bottom: 15),
                        color: Colors.white,
                        width: double.infinity,
                        child: Center(
                            child: Column(
                              children: <Widget>[
                                Loader(),
                                SizedBox(
                                  height: 5,
                                ),
//                                Text("Cargando Lugares turísticos.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
//                                Text("Espere por favor.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
                              ],
                            )
                        ),
                      );
                    } else {
                      if (snapshot.data != null) {
                        List<AgendaModel.Agenda> lugares = snapshot.data;
                        dataAgenda = lugares.where((lugar) => lugar.estado == 1).toList();
                      }

                      List<Widget> itemsLugares = [];
                      for(int index = 0; index < dataAgenda.length; index++) {
                        itemsLugares.add(
                          GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ImageSelectedWidget(
                                      pathImage: "${widget.apiBaseUrl}/img/${dataAgenda[index].imagen}",
                                    )
                                )
                                );
                              },
                              child:Padding(
                                padding: EdgeInsets.only(bottom: 10, left: 20, right: 20),
                                child: Card(
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(context).size.width,
                                        height: 300,
                                        decoration: BoxDecoration(color: Colors.blueAccent),
                                        child: FadeInImage.assetNetwork(
                                          placeholder: 'assets/loading-places.gif',
                                          image: "${widget.apiBaseUrl}/img/${dataAgenda[index].imagen}",
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Positioned(
                                        child: Padding(
                                          padding: EdgeInsets.all(0),
                                          child: Container(
                                            height: 300,
                                            width: MediaQuery.of(context).size.width,
                                            decoration: BoxDecoration(
                                                gradient: LinearGradient(
                                                    begin: Alignment.topLeft,
                                                    end: Alignment.bottomLeft,
                                                    stops: [
                                                      0,
                                                      .8
                                                    ],
                                                    colors: [
                                                      Colors.white.withOpacity(0),
                                                      Colors.black.withOpacity(.75)
                                                    ])),
                                          ),
                                        ),
                                        bottom: 0,
                                      ),
                                      Positioned(
                                        width: MediaQuery.of(context).size.width,
                                        child: Padding(
                                          padding: EdgeInsets.all(15),
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                child: Text(dataAgenda[index].nombre,
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w700,
                                                      fontSize: 24,

                                                    )
                                                ),
                                                padding: EdgeInsets.only(right: 35),
                                                width: MediaQuery.of(context).size.width,
                                              ),
                                            ],
                                          ),
                                        ),
                                        bottom: 0,
                                      ),
                                    ],
                                  ),
                                ),
                              )
                          )


                        );
                      }

                      return Container(
                        color: Colors.white,
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          children: itemsLugares,
                        ),
//                        child: ListView.builder(
//                            itemExtent: 300,
//                            itemCount: dataLugares.length,
//                            itemBuilder: (BuildContext context, int index) {
//                              return Container(
//                                child: Card(
//                                  clipBehavior: Clip.antiAliasWithSaveLayer,
//                                  child: Stack(
//                                    children: <Widget>[
//                                      Container(
//                                        width: MediaQuery.of(context).size.width,
//                                        height: MediaQuery.of(context).size.height,
//                                        decoration: BoxDecoration(color: Colors.blueAccent),
//                                        child: FadeInImage.assetNetwork(
//                                          placeholder: 'assets/loading-places.gif',
//                                          image: "${widget.apiBaseUrl}/img/${dataLugares[index].imagen}",
//                                          fit: BoxFit.cover,
//                                        ),
//                                      ),
//                                      Positioned(
//                                        child: Padding(
//                                          padding: EdgeInsets.all(0),
//                                          child: Container(
//                                            height: 300,
//                                            width: MediaQuery.of(context).size.width,
//                                            decoration: BoxDecoration(
//                                                gradient: LinearGradient(
//                                                    begin: Alignment.topLeft,
//                                                    end: Alignment.bottomLeft,
//                                                    stops: [
//                                                      0,
//                                                      .6
//                                                    ],
//                                                    colors: [
//                                                      Colors.white.withOpacity(0),
//                                                      Colors.black.withOpacity(.85)
//                                                    ])),
//                                          ),
//                                        ),
//                                        bottom: 0,
//                                      ),
//                                      Positioned(
//                                        width: MediaQuery.of(context).size.width,
//                                        child: Padding(
//                                          padding: EdgeInsets.all(15),
//                                          child: Column(
//                                            children: <Widget>[
//                                              Container(
//                                                child: Text(dataLugares[index].nombre,
//                                                    textAlign: TextAlign.left,
//                                                    style: TextStyle(
//                                                      color: Colors.white,
//                                                      fontWeight: FontWeight.w700,
//                                                      fontSize: 24,
//
//                                                    )
//                                                ),
//                                                width: MediaQuery.of(context).size.width,
//                                              ),
//                                              Row(
//                                                children: <Widget>[
//                                                  Text('Dirección:  ',
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w700,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                  Text(dataLugares[index].direccion,
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w300,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                ],
//                                              ),
//                                              Row(
//                                                children: <Widget>[
//                                                  Text('Descripción:  ',
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w700,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                  Text(dataLugares[index].horario,
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w300,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                ],
//                                              ),
//                                            ],
//                                          ),
//                                        ),
//                                        bottom: 0,
//                                      ),
//                                    ],
//                                  ),
//                                ),
//                              );
//                            }),
                      );
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}