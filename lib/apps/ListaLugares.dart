import 'package:flutter/material.dart';

import 'LugarSeleccionado.dart';

import 'dart:math';

class ListaLugares extends StatefulWidget {
  const ListaLugares({
    Key key,
    @required this.data,
    @required this.pathImages,
  }) : super(key: key);

  final List data;
  final String pathImages;

  @override
  _ListaLugaresState createState() => new _ListaLugaresState();
}

class _ListaLugaresState extends State<ListaLugares> {
  List data;
  String pathImages;

  @override
  void initState() {
    super.initState();
    data = widget.data;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            title: Text('Lista de lugares'),
            flexibleSpace: FlexibleSpaceBar(),
          ),
          SliverFixedExtentList(
              itemExtent: 140,
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => LugarSeleccionado(
                          data: data[index], pathImages: pathImages),
                    ));
                  },
                  child: Card(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(color: Colors.blueAccent),
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/loading-places.gif',
                            image: "$pathImages/img/${data[index].galeria[new Random().nextInt(data[index].galeria.length - 1)]["imagen"]}",
                            fit: BoxFit.cover,
                          ),
                        ),
                        Positioned(
                          child: Padding(
                            padding: EdgeInsets.all(0),
                            child: Container(
                              height: 100,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomLeft,
                                      stops: [
                                    0,
                                    .7
                                  ],
                                      colors: [
                                    Colors.white.withOpacity(0),
                                    Colors.black.withOpacity(.5)
                                  ])),
                            ),
                          ),
                          bottom: 0,
                        ),
                        Positioned(
                          child: Padding(
                            padding: EdgeInsets.all(15),
                            child: Text(data[index].nombre,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 24,
                                )),
                          ),
                          bottom: 0,
                        ),
                      ],
                    ),
                  ),
                );
              }, childCount: data.length))
        ],
      ),
    );
  }
}
