import 'package:flutter/material.dart';

import 'LugarSeleccionado.dart';
import 'package:app_turismo_pasaje/components/ImageSelected.dart';


class ListaServicios extends StatefulWidget {
  const ListaServicios({
    Key key,
    @required this.data,
    @required this.pathImages,
  }) : super(key: key);

  final List data;
  final String pathImages;

  @override
  _ListaServiciosState createState() => new _ListaServiciosState();
}

class _ListaServiciosState extends State<ListaServicios> {
  List data;
  String pathImages;

  @override
  void initState() {
    super.initState();
    data = widget.data;
    pathImages = widget.pathImages;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            title: Text('Lista de servicios turísticos'),
            flexibleSpace: FlexibleSpaceBar(),
          ),
          SliverFixedExtentList(
              itemExtent: 300,
              delegate:
                  SliverChildBuilderDelegate((BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ImageSelectedWidget(
                          pathImage: "$pathImages/img/${data[index].imagen}",
                        )
                    )
                    );
                },
                  child: Card(
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(color: Colors.blueAccent),
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/loading-places.gif',
                            image: "$pathImages/img/${data[index].imagen}",
                            fit: BoxFit.cover,
                          ),
                        ),
                        Positioned(
                          child: Padding(
                            padding: EdgeInsets.all(0),
                            child: Container(
                              height: 300,
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomLeft,
                                      stops: [
                                    0,
                                    .8
                                  ],
                                  colors: [
                                    Colors.white.withOpacity(0),
                                    Colors.black.withOpacity(.75)
                                  ])),
                            ),
                          ),
                          bottom: 0,
                        ),
                        Positioned(
                          width: MediaQuery.of(context).size.width,
                          child: Padding(
                            padding: EdgeInsets.all(15),
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Text(data[index].nombre,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 24,

                                    )
                                  ),
                                  padding: EdgeInsets.only(right: 35),
                                  width: MediaQuery.of(context).size.width,
                                ),
                                Row(
                                  children: <Widget>[
                                    Text('Categoría:  ',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 15,
                                        )
                                    ),
                                    Text(data[index].categoria,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w300,
                                          fontSize: 15,
                                        )
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text('Dirección:  ',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 15,
                                        )
                                    ),
                                    Expanded(
                                      child: Text(data[index].direccion,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w300,
                                            fontSize: 15,
                                          )
                                      ),
                                    ),

                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text('Teléfono:  ',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 15,
                                        )
                                    ),
                                    Text(data[index].telefono,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w300,
                                          fontSize: 15,
                                        )
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text('Horario:  ',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 15,
                                        )
                                    ),
                                    Text(data[index].horario,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w300,
                                          fontSize: 15,
                                        )
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          bottom: 0,
                        ),
                      ],
                    ),
                  ),
                );
              }, childCount: data.length))
        ],
      ),
    );
  }
}
