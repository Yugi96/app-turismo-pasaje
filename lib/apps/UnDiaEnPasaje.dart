import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:app_turismo_pasaje/apps/ListaPuntos.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetImage.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetText.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetLinearGradient.dart';
import 'package:app_turismo_pasaje/models/Ruta.dart';
import 'package:app_turismo_pasaje/components/loader.dart';

import 'ListaServicios.dart';

class UnDiaEnPasaje extends StatefulWidget {
  const UnDiaEnPasaje({
    Key key,
    @required this.apiBaseUrl,
  }) : super(key: key);

  final String apiBaseUrl;

  @override
  _UnDiaEnPasaje createState() => _UnDiaEnPasaje();
}

class _UnDiaEnPasaje extends State<UnDiaEnPasaje> {
  double topOne = 0;
  double topTwo = 0;
  double topThree = 0;
  bool isChange = false;
  bool isVisible = false;

  int count = 0;

  String url = "";
  List dataRutas;

  @override
  void initState() {
    super.initState();
    count = 0;
    url = "${widget.apiBaseUrl}/api/rutas";
  }

  Future<List<Ruta>> _getRutas() async {
    if (dataRutas == null && count == 0) {
      count++;
      var response = await http.get(
          Uri.encodeFull(url),
          headers: { "Accept": "application/json" }
      );

      var convertDataToJson = jsonDecode(response.body);

      List<Ruta> rutasList = [];

      for (var s in convertDataToJson) {
        Ruta rutas = Ruta(s["id"], s["nombre"], s["descripcion"], s["estado"], s["puntos"]);
        rutasList.add(rutas);
      }
      dataRutas = rutasList.where((ruta) => ruta.estado == 1 && ruta.puntos.length != 0).toList();
      return rutasList;
    } else {
      return dataRutas;
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification && v.depth == 0) {
            setState(() {
              topOne = topOne - v.scrollDelta / 1;
              topTwo = topTwo - v.scrollDelta / 3;
              topThree = topThree - v.scrollDelta / 1;
            });
            isChange = v.metrics.pixels > 200;
            isVisible = v.metrics.pixels > 200;
          }
          return true;
        },
        child: Stack(
          children: <Widget>[
            new ParallaxWidgetImage(top: topTwo, asset: "J"),
            new ParallaxWidgetLinearGradient(top: topThree, isVisible: isVisible),
            new ParallaxWidgetText(top: topOne, isChange: isChange, title: "Un día en pasaje", description: "Te invitamos aventurarte en las diferentes rutas turísticas para planear su viaje por Pasaje",),
            ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent,
                ),
                FutureBuilder(
                  future: _getRutas(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {

                    if (snapshot.data == null && dataRutas == null) {
                      return Container(
                        padding: EdgeInsets.only(top: 5, bottom: 15),
                        color: Colors.white,
                        width: double.infinity,
                        child: Center(
                            child: Column(
                              children: <Widget>[
                                Loader(),
                                SizedBox(
                                  height: 5,
                                ),
//                                Text("Cargando Lugares turísticos.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
//                                Text("Espere por favor.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
                              ],
                            )
                        ),
                      );
                    } else {
                      if (snapshot.data != null) {
                        List<Ruta> rutas = snapshot.data;
                        dataRutas = rutas.where((ruta) => ruta.estado == 1 && ruta.puntos.length != 0).toList();
                      }

                      List<Widget> itemsLugares = [];
                      for(int index = 0; index < dataRutas.length; index++) {
                        itemsLugares.add(
                          GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => ListaPuntos(data: dataRutas[index].puntos, ruta: dataRutas[index].nombre, pathImages: widget.apiBaseUrl,),
                                  )
                              );
                            },
                            child: Padding(
                              padding: EdgeInsets.only(bottom: 10, left: 20, right: 20),
                              child: Card(
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 300,
                                      decoration: BoxDecoration(color: Colors.blueAccent),
                                      child: FadeInImage.assetNetwork(
                                        placeholder: 'assets/loading-places.gif',
                                        image: "${widget.apiBaseUrl}/img/${dataRutas[index].puntos[0]["imagen"]}",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Positioned(
                                      child: Padding(
                                        padding: EdgeInsets.all(0),
                                        child: Container(
                                          height: 300,
                                          width: MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  begin: Alignment.topLeft,
                                                  end: Alignment.bottomLeft,
                                                  stops: [
                                                    0,
                                                    .7
                                                  ],
                                                  colors: [
                                                    Colors.white.withOpacity(0),
                                                    Colors.black.withOpacity(.75)
                                                  ])),
                                        ),
                                      ),
                                      bottom: 0,
                                    ),
                                    Positioned(
                                      width: MediaQuery.of(context).size.width,
                                      child: Padding(
                                        padding: EdgeInsets.all(15),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                              child: Text(dataRutas[index].nombre,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: 24,

                                                  )
                                              ),
                                              padding: EdgeInsets.only(right: 35),
                                              width: MediaQuery.of(context).size.width,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: Container(
                                                  padding: EdgeInsets.only(right: 40),
                                                    width: MediaQuery.of(context).size.width * 0.70,
                                                    child: Text(dataRutas[index].descripcion,
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight: FontWeight.w300,
                                                          fontSize: 13,
                                                        )
                                                    ),
                                                  ),
                                                ),

                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      bottom: 0,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )

                        );
                      }

                      return Container(
                        color: Colors.white,
                        width: double.infinity,
//                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          children: itemsLugares,
                        ),
//                        child: ListView.builder(
//                            itemExtent: 300,
//                            itemCount: dataLugares.length,
//                            itemBuilder: (BuildContext context, int index) {
//                              return Container(
//                                child: Card(
//                                  clipBehavior: Clip.antiAliasWithSaveLayer,
//                                  child: Stack(
//                                    children: <Widget>[
//                                      Container(
//                                        width: MediaQuery.of(context).size.width,
//                                        height: MediaQuery.of(context).size.height,
//                                        decoration: BoxDecoration(color: Colors.blueAccent),
//                                        child: FadeInImage.assetNetwork(
//                                          placeholder: 'assets/loading-places.gif',
//                                          image: "${widget.apiBaseUrl}/img/${dataLugares[index].imagen}",
//                                          fit: BoxFit.cover,
//                                        ),
//                                      ),
//                                      Positioned(
//                                        child: Padding(
//                                          padding: EdgeInsets.all(0),
//                                          child: Container(
//                                            height: 300,
//                                            width: MediaQuery.of(context).size.width,
//                                            decoration: BoxDecoration(
//                                                gradient: LinearGradient(
//                                                    begin: Alignment.topLeft,
//                                                    end: Alignment.bottomLeft,
//                                                    stops: [
//                                                      0,
//                                                      .6
//                                                    ],
//                                                    colors: [
//                                                      Colors.white.withOpacity(0),
//                                                      Colors.black.withOpacity(.85)
//                                                    ])),
//                                          ),
//                                        ),
//                                        bottom: 0,
//                                      ),
//                                      Positioned(
//                                        width: MediaQuery.of(context).size.width,
//                                        child: Padding(
//                                          padding: EdgeInsets.all(15),
//                                          child: Column(
//                                            children: <Widget>[
//                                              Container(
//                                                child: Text(dataLugares[index].nombre,
//                                                    textAlign: TextAlign.left,
//                                                    style: TextStyle(
//                                                      color: Colors.white,
//                                                      fontWeight: FontWeight.w700,
//                                                      fontSize: 24,
//
//                                                    )
//                                                ),
//                                                width: MediaQuery.of(context).size.width,
//                                              ),
//                                              Row(
//                                                children: <Widget>[
//                                                  Text('Dirección:  ',
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w700,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                  Text(dataLugares[index].direccion,
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w300,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                ],
//                                              ),
//                                              Row(
//                                                children: <Widget>[
//                                                  Text('Descripción:  ',
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w700,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                  Text(dataLugares[index].horario,
//                                                      style: TextStyle(
//                                                        color: Colors.white,
//                                                        fontWeight: FontWeight.w300,
//                                                        fontSize: 15,
//                                                      )
//                                                  ),
//                                                ],
//                                              ),
//                                            ],
//                                          ),
//                                        ),
//                                        bottom: 0,
//                                      ),
//                                    ],
//                                  ),
//                                ),
//                              );
//                            }),
                      );
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}