import 'package:flutter/material.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:app_turismo_pasaje/components/CarouselCategoriasWidget.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetImage.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetText.dart';
import 'package:app_turismo_pasaje/components/ParallaxWidgetLinearGradient.dart';
import 'package:app_turismo_pasaje/models/servicio.dart';
import 'package:app_turismo_pasaje/apps/ListaLugares.dart';
import 'package:app_turismo_pasaje/components/loader.dart';

import 'ListaServicios.dart';

class ServiciosTuristicos extends StatefulWidget {
  const ServiciosTuristicos({
    Key key,
    @required this.apiBaseUrl,
  }) : super(key: key);

  final String apiBaseUrl;

  @override
  _ServiciosTuristicos createState() => _ServiciosTuristicos();
}

class _ServiciosTuristicos extends State<ServiciosTuristicos> {
  double topOne = 0;
  double topTwo = 0;
  double topThree = 0;
  bool isChange = false;
  bool isVisible = false;

  int count = 0;

  String url = "";
  List dataServicios;
  List categories = [];

  @override
  void initState() {
    super.initState();
    count = 0;
    url = "${widget.apiBaseUrl}/api/servicios";
  }

  Future<List<Servicio>> _getSeriviosTuristicos() async {
    if (dataServicios == null && count == 0) {
      count++;
      var response = await http.get(
          Uri.encodeFull(url),
          headers: { "Accept": "application/json" }
      );

      var convertDataToJson = jsonDecode(response.body);

      List<Servicio> servicioList = [];

      for (var s in convertDataToJson) {
        Servicio servicios = Servicio(s["id"], s["nombre"], s["direccion"], s["telefono"], s["horario"], s["categoria"], s["imagen"], s["estado"]);
        servicioList.add(servicios);
      }
      dataServicios = servicioList.where((servicio) => servicio.estado == 1).toList();
      return servicioList;
    } else {
      return dataServicios;
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: NotificationListener(
        onNotification: (v) {
          if (v is ScrollUpdateNotification && v.depth == 0) {
            setState(() {
              topOne = topOne - v.scrollDelta / 1;
              topTwo = topTwo - v.scrollDelta / 3;
              topThree = topThree - v.scrollDelta / 1;
            });
            isChange = v.metrics.pixels > 200;
            isVisible = v.metrics.pixels > 200;
          }
          return true;
        },
        child: Stack(
          children: <Widget>[
            new ParallaxWidgetImage(top: topTwo, asset: "S"),
            new ParallaxWidgetLinearGradient(top: topThree, isVisible: isVisible),
            new ParallaxWidgetText(top: topOne, isChange: isChange, title: "Servicios turísticos", description: "Información de lugares de hospedaje, centros de entretenimiento y diversión, además de la caracterización de los principales restaurantes, cafeterías, pizzerías y cevicheras del cantón.",),
            ListView(
              padding: EdgeInsets.all(0),
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent,
                ),
                FutureBuilder(
                  future: _getSeriviosTuristicos(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {

                    if (snapshot.data == null && dataServicios == null) {
                      return Container(
                        padding: EdgeInsets.only(top: 5, bottom: 15),
                        color: Colors.white,
                        width: double.infinity,
                        child: Center(
                            child: Column(
                              children: <Widget>[
                                Loader(),
                                SizedBox(
                                  height: 5,
                                ),
//                                Text("Cargando Lugares turísticos.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
//                                Text("Espere por favor.", style: TextStyle(
//                                  fontWeight: FontWeight.w300,
//                                  fontSize: 12,
//                                ),),
                              ],
                            )
                        ),
                      );
                    } else {
                      if (snapshot.data != null) {
                        List<Servicio> servicios = snapshot.data;
                        dataServicios = servicios.where((servicio) => servicio.estado == 1).toList();

                        categories = [];
                        for (var servicio in dataServicios) {
                          var exists = categories.singleWhere((category) => category["categoria"] == servicio.categoria, orElse: () => null);
                          if (exists == null) {
                            categories.add({ "categoria": servicio.categoria, "imagen": servicio.imagen });
                          }
                        }
                      }

                      return Container(
                        color: Colors.white,
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(left: 20.0),
                              child: Text(
                                "CATEGORÍA",
                                style: TextStyle(
                                  fontSize: 16,
                                  letterSpacing: 1.2,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            CarouselCategoriasWidget(services: dataServicios, categories: categories, pathImages: widget.apiBaseUrl,),
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  child: RaisedButton(
                                      color: Colors.blue,
                                      textColor: Colors.white,
                                      disabledColor: Colors.grey,
                                      disabledTextColor: Colors.black,
                                      padding: EdgeInsets.symmetric(horizontal: 50),
                                      splashColor: Colors.blueAccent,
                                      elevation: 2,
                                      onPressed: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (context) => ListaServicios(data: dataServicios, pathImages: widget.apiBaseUrl,),
                                            )
                                        );
                                      },
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "VER TODOS",
                                            style: TextStyle(fontSize: 12.0),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Icon(
                                            Icons.add,
                                            size: 16,
                                          )
                                        ],
                                      )
                                  ),
                                )

                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}