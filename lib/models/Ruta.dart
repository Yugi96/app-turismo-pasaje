class Ruta {
  final id;
  final nombre;
  final descripcion;
  final puntos;
  final estado;

  Ruta(this.id, this.nombre, this.descripcion, this.estado, this.puntos);

}
