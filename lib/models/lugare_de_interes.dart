class LugarDeInteres {
  final id;
  final nombre;
  final descripcion;
  final direccion;
  final imagen;
  final estado;

  LugarDeInteres(this.id, this.nombre, this.descripcion, this.direccion,
      this.imagen, this.estado);

}
