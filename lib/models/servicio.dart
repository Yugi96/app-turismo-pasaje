class Servicio {
  final id;
  final nombre;
  final direccion;
  final telefono;
  final horario;
  final categoria;
  final imagen;
  final estado;

  Servicio(this.id, this.nombre, this.direccion, this.telefono, this.horario,
      this.categoria, this.imagen, this.estado);

}
