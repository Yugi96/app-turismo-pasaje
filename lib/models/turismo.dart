class Turismo {
  final id;
  final nombre;
  final descripcion;
  final ubicacion;
  final distancia;
  final temperatura;
  final longitud;
  final latitud;
  final es_importante;
  final galeria;
  final actividades;
  final parroquia;
  final estado;

  Turismo(this.id, this.nombre, this.descripcion, this.ubicacion,
      this.distancia, this.temperatura, this.longitud, this.latitud,
      this.es_importante, this.galeria, this.actividades, this.parroquia,
      this.estado);


}