class Parroquia {
  final id;
  final nombre;
  final imagen;
  final fundacion;
  final ubicacion;
  final superficie;
  final poblacion;
  final distancia_de_pasaje;
  final actividades;
  final estado;

  Parroquia(this.id, this.nombre, this.imagen, this.fundacion, this.ubicacion,
      this.superficie, this.poblacion, this.distancia_de_pasaje,
      this.actividades, this.estado);

}