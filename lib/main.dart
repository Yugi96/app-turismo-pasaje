import 'package:flutter/material.dart';
import 'package:app_turismo_pasaje/app_config.dart';
import 'package:app_turismo_pasaje/apps/QueConocer.dart';
import 'package:app_turismo_pasaje/apps/ServiciosTuristicos.dart';
import 'package:app_turismo_pasaje/apps/LugaresInteres.dart';
import 'package:app_turismo_pasaje/apps/Agenda.dart';
import 'package:app_turismo_pasaje/apps/UnDiaEnPasaje.dart';

import 'apps/Pasaje.dart';

void main() {
  AppConfig configuredApp = new AppConfig(
    appName: 'Build flavors',
    apiBaseUrl: 'http://municipiodepasaje.gob.ec',
//    apiBaseUrl: 'https://pasajeturismo.optmanager.com',
    child: new MainApp(),
  );
  runApp(configuredApp);
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: ThemeData(fontFamily: 'Lato'),
      home: Home(),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AppConfig config = AppConfig.of(context);

    return new PageView(
      controller: PageController(
        initialPage: 0
      ),
      children: <Widget>[
        Pasaje(apiBaseUrl: config.apiBaseUrl,),
        QueConocer(apiBaseUrl: config.apiBaseUrl,),
        ServiciosTuristicos(apiBaseUrl: config.apiBaseUrl,),
        LugaresInteres(apiBaseUrl: config.apiBaseUrl,),
        UnDiaEnPasaje(apiBaseUrl: config.apiBaseUrl,),
        Agenda(apiBaseUrl: config.apiBaseUrl,),
      ],
    );
  }
}



